﻿using System;
using System.Globalization;

namespace ConsoleTask
{
    internal class UserInputReader
    {
        public static Transaction ReadTransaction()
        {
            int id = ReadId();

            Console.Write("Введите дату:");
            if (!DateTime.TryParse(Console.ReadLine(), out DateTime date))
                throw new FormatException("Недопустимая дата!");

            Console.Write("Введите сумму:");
            if (!decimal.TryParse(Console.ReadLine(), NumberStyles.Any, CultureInfo.InvariantCulture, out decimal amount))
                throw new FormatException("Недопустимая сумма!");

            var transaction = new Transaction
            {
                Id = id,
                TransactionDate = date,
                Amount = amount
            };

            return transaction;
        }

        public static int ReadId()
        {
            Console.Write("Введите Id:");
            if (!int.TryParse(Console.ReadLine(), NumberStyles.Any, CultureInfo.InvariantCulture, out int id))
                throw new FormatException("Недопустимый идентификатор!");
            return id;
        }
    }
}
