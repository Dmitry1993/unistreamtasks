﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace ConsoleTask
{
    class Program
    {
        static void Main(string[] args)
        {
            bool working = true;

            using (TransactionContext context = new TransactionContext())
            {
                while (working)
                {
                    string command = Console.ReadLine();

                    switch (command)
                    {
                        case "add":
                            AddTransactionToDb(context);
                            break;

                        case "get":
                            GetTransactionFromDb(context);
                            break;

                        case "exit":
                            working = false;
                            break;

                        default:
                            ShowHelp();
                            break;
                    }
                }
            }
        }

        private static void AddTransactionToDb(TransactionContext context)
        {
            Transaction transaction;
            try
            {
                transaction = UserInputReader.ReadTransaction();
            }
            catch (FormatException e)
            {
                Console.WriteLine("Ошибка во входных данных: " + e.Message);
                return;
            }

            try
            {
                context.Transactions.Add(transaction);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при записи в БД: " + e.Message);
                return;
            }

            Console.WriteLine("[OK]");
        }

        private static void GetTransactionFromDb(TransactionContext context)
        {
            int id;
            try
            {
                id = UserInputReader.ReadId();
            }
            catch (FormatException e)
            {
                Console.WriteLine("Ошибка во входных данных: " + e.Message);
                return;
            }

            var transaction = context.Transactions.FirstOrDefault(t => t.Id == id);
            Console.WriteLine(JsonConvert.SerializeObject(transaction));
        }

        private static void ShowHelp()
        {
            using (StreamReader stream = new StreamReader("instruction.txt"))
            {
                string instruction = stream.ReadToEnd();
                Console.WriteLine(instruction);
            }
        }
    }
}
