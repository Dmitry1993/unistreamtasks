﻿using System.Data.Entity;

namespace ConsoleTask
{
    public class TransactionContext : DbContext
    {
        public TransactionContext() : base("TransactionDB")
        { }

        public DbSet<Transaction> Transactions { get; set; }
    }
}
