﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleTask
{
    public class Transaction
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // Отключение автоинкремента средствами БД
        public int Id { get; set; }

        public DateTime TransactionDate { get; set; }

        public decimal Amount { get; set; }
    }
}
