﻿using System.Data.Entity;

namespace WebTask.Models
{
    public class EntityContext : DbContext
    {
        public EntityContext() : base("EntityDB")
        {
        }

        public DbSet<Entity> Entities { get; set; }
    }
}