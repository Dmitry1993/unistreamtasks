﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using WebTask.Models;

namespace WebTask.Controllers
{
    public class EntityController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Add(string insert)
        {
            try
            {
                var entity = JsonConvert.DeserializeObject<Entity>(insert);

                using (EntityContext context = new EntityContext())
                {
                    context.Entities.Add(entity);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        [HttpGet]
        public IHttpActionResult Read(string get)
        {
            try
            {
                var id = Guid.Parse(get);

                using (EntityContext context = new EntityContext())
                {
                    var entity = context.Entities.FirstOrDefault(t => t.Id == id);
                    string json = JsonConvert.SerializeObject(entity);
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, json));
                }
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}
